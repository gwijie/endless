<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;


class SubmittedPageController extends Controller
{
    public function index(){
        
        $data['address'] = Address::first();
        $data['pageName'] = 'Survey';
        $data['surveyActive'] = 'active';
        
        return view('pages.submitted')->with($data);
    }
}
