<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LandingPages;
use App\About;
use App\Values;
use App\Statistic;
use App\Address;

class LandingPageController extends Controller
{
    public function index(){
        $data['landing'] = LandingPages::first();
        //$data['about'] = About::first();
        //$data['values'] = Values::get()->take(4);
        $data['facts'] = Statistic::get()->take(4);
        $data['address'] = Address::first();

        return view('pages.landingpage')->with($data);
    }
}
