<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm;
use Carbon\Carbon;
use App\Survey;
use Session;

class ContactPageController extends Controller
{
    public function submit_contact(Request $request){
        $fields = $request->all();
        $data = array(
            'name' => $fields['name'],
            'email' => $fields['email'],
            'msg' => $fields['msg']
        );
        Mail::to('me@gwijie.com')
              ->bcc('lacoasta@gmail.com')
              ->send(new ContactForm($data));

        Session::flash('messages','<div class="alert-success" style="padding: 5px;
                    margin: 7px 0 4px 0;
                    border-radius: 5px;
                    font-weight: bold;">
                    <center>Thank you for reaching out; We will get back to you shortly</center></div>');
        return redirect('/#hello');
                
       
    }
}
