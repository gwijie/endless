<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailSurveyResults;
use Carbon\Carbon;
use App\Survey;
use App\Address;
use Session;

class SurveyPageController extends Controller
{

    public function index(){

        $data['address'] = Address::first();
        $data['pageName'] = 'Survey';
        $data['surveyActive'] = 'active';

        return view('pages.surveypage')->with($data);
    }

    public function submitted(Request $request){

        $fields = $request->all();
        $data = array(
            'name' => $fields['name'],
            'email' => $fields['email'],
            'gender' => $fields['gender'],
            'age' => $fields['age'],
            'house_currently_live_in' => $fields['house'],
            'currently_pay_rent' => $fields['live'],
            'current_monthly_rent' => $fields['rent'],
            'zones' => $fields['zone'],
            'rooms' => $fields['rooms'],
            'monthly_savings' => $fields['save'],
            'sources_to_raise_deposit' => $fields['access'],
            'purchase_date' => $fields['buy'],
            'tel' => $fields['tel'],
            'created_at' => Carbon::now()
        ); 


        $i = Survey::insert($data);

        Mail::to($fields['email'])
              ->bcc('lacoasta@gmail.com')
              ->send(new MailSurveyResults($data));

        if($i > 0)
                {
                    Session::flash('messages','<div class="alert-success" style="padding: 5px;
                    margin: 7px 0 4px 0;
                    border-radius: 5px;
                    font-weight: bold;">
                    <center>Your results will be sent out to you email shortly</center></div>');
                    return redirect('/survey_report');
                }

    }
}
