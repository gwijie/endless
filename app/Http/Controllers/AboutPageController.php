<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;
use App\Values;
use App\Address;

class AboutPageController extends Controller
{
    public function index(){

        $data['pageName'] = "About Us";
        $data['aboutActive'] = "active";
        $data['about'] = About::first();
        $data['values'] = Values::get()->take(4);
        $data['address'] = Address::first();

        return view('pages.about')->with($data);
    }
}
