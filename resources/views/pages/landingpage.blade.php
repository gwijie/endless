@extends('template.template')

@section('content')

        @include('partials.landing')

        @include('partials.facts')
        
        @include('partials.contacts')

        @include('partials.address')

@endsection