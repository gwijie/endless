<section id="contact" class="pt90 pb60 bg-faded">
            <div class="container">
                <div class="row" style="margin-top: 3em!important;">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4>Survey</h4>
                            <p class="lead">Spare less than a minute to take this short survey</p>
                            
                            <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="17" aria-valuemin="17" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div><!--/section title-->
               <div class="row">
                    <div class="col-md-12 mb30">
                        <form method="post" action="/submitted" data-toggle="validator" class="">
                        {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-12"> 
                                
                                <div class="house">
                                    <h3>What type of house do you currently live in?</h3><br>
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" checked="checked" type="radio" name="house" id="inlineRadio1" value="Mansion/Bangalow">
                                            <label class="form-check-label" for="inlineRadio1"> Mansion/Bungalow </label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Town House/Maisonette">
                                            <label class="form-check-label" for="inlineRadio1">Town House/Maisonette </label>
                                            </div>
                                        </div> 
                                        
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Thatched House/Mabati House">
                                            <label class="form-check-label" for="inlineRadio1">Thatched House/Mabati House</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Country Home/Rural Home">
                                            <label class="form-check-label" for="inlineRadio1">Country Home/Rural Home</label>
                                            </div>
                                        </div>
                                        
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Apartment/Flat">
                                            <label class="form-check-label" for="inlineRadio1">Apartment/Flat </label>
                                            </div>
                                            <div class="col">
                                            
                                            </div>
                                        </div>

                                    
                                    <div class="col-sm-12 text-right">
                                        
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextHouse">Next</button>
                                    </div>
                                    <br>
                                </div>

                                <div class="live">
                                    <h3>Do you currently pay rent? </h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                            <input class="form-check-input " type="radio" checked="checked" name="live" id="inlineRadio1" value="I own my current residence"> 
                                            <label class="form-check-label" for="inlineRadio1">I own my current residence</label>
                                        </div>

                                        <div class="col">
                                            <input class="form-check-input " type="radio" name="live" id="inlineRadio1" value="I am currently renting">
                                            <label class="form-check-label" for="inlineRadio1">I am currently renting</label>
</div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="live" id="inlineRadio1" value="None of the above">
                                        <label class="form-check-label" for="inlineRadio1"> None of the above (Dependent, Spouse, etc)</label>
                                        </div>
                                        <div class="col">
                                        

                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevLive">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextLive">Next</button>
                                    </div>
                                    <br>
                                </div>
                            
                                <div class="rent">    
                                    <h3>If you are currently renting, what is your current monthly rent? </h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" checked="checked" name="rent" id="inlineRadio1" value="Kes. 2,000 - 7,000">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 2,000 - 7,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="Kes. 8,000 - 15,000">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 8,000 - 15,000</label>
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="Kes. 15,000 - 30,000">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 15,000 - 30,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="Kes. 30,000 - 50,000">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 30,000 - 50,000</label>
                                        </div>
                                    </div>

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="Kes. 50,000 - 75,000">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 50,000 - 75,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="Over Kes. 75,000K">
                                        <label class="form-check-label" for="inlineRadio1">Over Kshs. 75,000</label>
                                        </div>
                                    </div>
                                    <div class="form-row" style="margin-left: 2em;">

                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="Not Applicable">
                                        <label class="form-check-label" for="inlineRadio1">Not Applicable </label>
                                        </div>
                                        <div class="col">
                                        
                                        </div>
                                    </div>

                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevRent">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextRent">Next</button>
                                    </div> 
                                    <br>
                                </div>
                                
                                <div class="zone">    
                                    <h3>Would you like to purchase your home in any of these Zones? </h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" checked="checked" name="zone" id="inlineRadio1" value="Zone A (Donholm, Fedha, Embakasi)">
                                        <label class="form-check-label" for="inlineRadio1">Zone A (Donholm, Fedha, Embakasi)</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="zone" id="inlineRadio1" value="Zone B (Syokimau, Mlolongo, Kitengela, Athi River)">
                                        <label class="form-check-label" for="inlineRadio1">Zone B (Syokimau, Mlolongo, Kitengela, Athi River)</label>
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="zone" id="inlineRadio1" value="Zone C (Ruaraka, Kahawa Sukari, Mwiki)">
                                        <label class="form-check-label" for="inlineRadio1">Zone C (Ruaraka, Kahawa Sukari, Mwiki)</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="zone" id="inlineRadio1" value="Zone D (Thindigua, Ruaka, Banana, Mwimuto)">
                                        <label class="form-check-label" for="inlineRadio1">Zone D (Thindigua, Ruaka, Banana, Mwimuto)</label>
                                        </div>
                                    </div>

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="zone" id="inlineRadio1" value="Zone E (Dagoretti, Uthiru, Kinoo, Kikuyu, Limuru)">
                                        <label class="form-check-label" for="inlineRadio1">Zone E (Dagoretti, Uthiru, Kinoo, Kikuyu, Limuru)</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="zone" id="inlineRadio1" value="Zone F (Rongai, Ngong, Kiserian)">
                                        <label class="form-check-label" for="inlineRadio1">Zone F (Rongai, Ngong, Kiserian)</label>
                                        </div>
                                    </div>
                                    <div class="form-row" style="margin-left: 2em;">

                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="zone" id="inlineRadio1" value="Other">
                                        <label class="form-check-label" for="inlineRadio1">Other </label>
                                        </div>
                                        <div class="col">
                                        
                                        </div>
                                    </div>

                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevZone">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextZone">Next</button>
                                    </div> 
                                    <br>
                                </div>
                                
                                <div class="rooms">
                                    <h3>What type of house would you like to purchase? (Housing prices dependent on location and size of units)</h3>
                                    <br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" checked="checked" name="rooms" id="inlineRadio1" value="Studio/bedsitter (Ksh 600,000 – 1,500,000)">
                                        <label class="form-check-label" for="inlineRadio1">Studio/bedsitter (Ksh 600,000 – 1,500,000)</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rooms" id="inlineRadio1" value="1 bedroom (Kshs 1,200,000 – 3,500,000)">
                                        <label class="form-check-label" for="inlineRadio1">1 bedroom (Kshs 1,200,000 – 3,500,000)</label>
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rooms" id="inlineRadio1" value="2 Bedroom (Kshs 2,000,000 – 5,000,000)">
                                        <label class="form-check-label" for="inlineRadio1">2 Bedroom (Kshs 2,000,000 – 5,000,000)</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rooms" id="inlineRadio1" value="3 Bedroom (Kshs 3,000,000 – 7,000,000)">
                                        <label class="form-check-label" for="inlineRadio1">3 Bedroom (Kshs 3,000,000 – 7,000,000)</label>
                                        </div>
                                    </div>
 
                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevRooms">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextRooms">Next</button>
                                    </div>
                                    <br>
                                </div>
                                
                                <div class="save">
                                    <h3>How much are you able to save monthly towards your deposit?
(Deposit required for home purchase is 10-20% of the value of the house, and would be saved for a maximum of 3 years)
</h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" checked="checked" name="save" id="inlineRadio1" value="Kes. 2,000 - 5,000">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 2,000 - 5,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="Kes. 6,000 - 10,000">
                                        <label class="form-check-label" for="inlineRadio1">Kes. 6,000 - 10,000</label>
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="Kes. 11,000 - 15,000">
                                        <label class="form-check-label" for="inlineRadio1">Kes. 11,000 - 15,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="Kes. 16,000 - 20,000">
                                        <label class="form-check-label" for="inlineRadio1">Kes. 16,000 - 20,000</label>
                                        </div>
                                    </div>

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="Kes. 21,000 - 30,000">
                                        <label class="form-check-label" for="inlineRadio1">Kes. 21,000 - 30,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="Over Kes. 30,000">
                                        <label class="form-check-label" for="inlineRadio1">Over Kes. 30,000</label>
                                        </div>
                                    </div> 
 
                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevSave">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextSave">Next</button>
                                    </div>
                                    <br>
                                </div>
                                
                                <div class="access"> 
                                        <h3>Other than monthly savings, how else will you raise your deposit?</h3><br>
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" checked="checked" name="access" id="inlineRadio1" value="Borrow from family/friends">
                                            <label class="form-check-label" for="inlineRadio1">Borrow from family/friends</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="access" id="inlineRadio1" value="Bank Loan">
                                            <label class="form-check-label" for="inlineRadio1">Bank Loan</label>
                                            </div>
                                        </div> 
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" checked="checked" name="access" id="inlineRadio1" value="Chama/SACCO Loan">
                                            <label class="form-check-label" for="inlineRadio1">Chama/SACCO Loan</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="access" id="inlineRadio1" value="Sell other assets/investments">
                                            <label class="form-check-label" for="inlineRadio1">Sell other assets/investments</label>
                                            </div>
                                            
                                        </div> 
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="access" id="inlineRadio1" value="Other">
                                            <label class="form-check-label" for="inlineRadio1">Other</label>
                                            </div>
                                            <div class="col">
                                            
                                            </div>
                                            <div class="col-sm-12 text-right">
                                            <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevAccess">Previous</button>
                                            <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextAccess">Next</button>
                                            </div>
                                        </div> 
                                    </div>
                                    
                                <div class="buy"> 
                                        <h3>How soon would you like to purchase your home? </h3><br>
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" checked="checked" name="buy" id="inlineRadio1" value="Right Away/Now">
                                            <label class="form-check-label" for="inlineRadio1">Right Away/Now</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="buy" id="inlineRadio1" value="3 – 6 Months from now">
                                            <label class="form-check-label" for="inlineRadio1">3 – 6 Months from now</label>
                                            </div>
                                        </div> 
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" checked="checked" name="buy" id="inlineRadio1" value="6 – 12 months from now">
                                            <label class="form-check-label" for="inlineRadio1">6 – 12 months from now</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="buy" id="inlineRadio1" value="1 – 2 years from now">
                                            <label class="form-check-label" for="inlineRadio1">1 – 2 years from now</label>
                                            </div>
                                            
                                        </div> 
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="buy" id="inlineRadio1" value="Over 2 years from now">
                                            <label class="form-check-label" for="inlineRadio1">Over 2 years from now</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="buy" id="inlineRadio1" value="I don’t want to buy a home">
                                            <label class="form-check-label" for="inlineRadio1">I don’t want to buy a home</label>
                                            </div>
                                            <div class="col-sm-12 text-right">
                                            <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevBuy">Previous</button>
                                            <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextBuy">Next</button>
                                            </div>
                                        </div> 
                                    </div>

                                <div class="submit"> 
                                        <h3>Enter your details to get your score</h3><br>
                                        <div class="form-row" style="margin-left: -1em;">
                                            
                                            <div class="col-sm-12">                                 
                                                <div class=" mb20">
                                                    <input type="text" name="name" class="form-control" placeholder="Full Name...." required/>
                                                </div>
                                                <div class=" mb20">
                                                    <input type="email" name="email" class="form-control" placeholder="Email Address...." required email />
                                                </div>
                                                <div class=" mb20">
                                                    <input type="text" name="tel" class="form-control" placeholder="Telephone No." required/>
                                                </div>
                                                <div class=" mb20">
                                                <h3>Age</h3><br>
                                                <div class="form-row" style="margin-left: 2em;">
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" checked="checked" name="age" id="inlineRadio1" value="18 - 25">
                                                        <label class="form-check-label" for="inlineRadio1">18 - 25 Yrs</label>
                                                        </div>
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="26 - 35">
                                                        <label class="form-check-label" for="inlineRadio1">26 - 35 Yrs</label>
                                                        </div>
                                                    </div> 
                                                    <div class="form-row" style="margin-left: 2em;">
                                                        
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="36 - 45">
                                                        <label class="form-check-label" for="inlineRadio1">36 - 45 Yrs</label>
                                                        </div>
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="46 - 55">
                                                        <label class="form-check-label" for="inlineRadio1">46 - 55 Yrs</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-row" style="margin-left: 2em;">
                                                        
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="55+">
                                                        <label class="form-check-label" for="inlineRadio1">Over 55 Yrs</label>
                                                        </div>
                                                        <div class="col">
                                                        
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <div class=" mb20">
                                                <h3>Gender</h3><br>
                                                    <select name="gender" class="form-control">
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>                          
                                            </div>

                                            <div class="col-sm-12 text-right">
                                                <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevSubmit">Previous</button>
                                                <button type="submit" name="submit" class="btn btn-lg btn-primary" id="nextAge">Finish</button>
                                            </div>
                                        </div> 
                                    </div>
                                    
                            </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="data-status"></div> <!-- data submit status -->
                                    
                                </div>
                            </div>
                        </form><!--form end-->
                    </div>
                </div>

            </div>
        </section><!--/.contact-->