<section id="about" class="pt90 pb60">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4 style="color: #ffcc00!important;">About Us</h4>
                        </div>
                    </div>
                </div><!--/section title-->

                <div class="row mb-20">
                    <div class="col-md-4">
                        <h3 class="d-md-none d-lg-none d-none">Endless Africa</h3>
                        <h3 style="color: #ffcc00!important;">Who We Are </h3>    
                        <p class="mb-40">
                            {!! $about['who_we_are'] !!}
                        </p>
                    </div>

                    <div class="col-md-8">
                        <h3 style="color: #ffcc00!important;">Our Mission </h3>
                        <p>{{ $about['mission']}}</p>
                        <br>
                        <h3 style="color: #ffcc00!important;">Our Vision</h3>
                        <p>{{ $about['vision']}}</p>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color: #ffcc00!important;">Why We Exist </h3>  
                        <p class="mb-20">
                        {!! $about['why_we_exist'] !!}
                        </p>
                    </div>
                    <br>
                </div>
                <br>
                
                @include('partials.values')
                
            </div>
</section><!--about us-->
