<div class="parallax-cta pt90 pb60 bg-parallax" data-jarallax='{"speed": 0.2}' style='background-image: url("/images/bg6.jpg")'>
            <div class="container">
                <div class="row">
                    @foreach($facts as $fact)
                    <div class="col-lg-3 col-md-6 mb30 wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="media fun-facts">
                            <div class="d-flex align-self-center mr-4">
                                {!! $fact->icon !!}
                            </div>
                            <div class="media-body">
                                <h3 class="mt-0 text-uppercase display-4 text-white" style="color: #ffcc00!important;">{{ $fact->stats }}</h3>
                                <p class='mb0 text-small text-uppercase'>{{ $fact->description  }}
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                    @endforeach
                </div>
            </div>      
        </div><!--/.fun facts-->