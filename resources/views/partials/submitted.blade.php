<section id="contact" class="pt90 pb60 bg-faded">
            <div class="container">
                <div class="row" style="margin-top: 3em;">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4>Survey</h4>
                            
                            <?php echo Session::get('messages');?>
                            <br>

                            <a class="btn btn-primary" href="/">Take me home</a>
                            
                        </div>
                    </div>
                </div><!--/section title-->
               <div class="row">
                    <div class="col-md-12 mb30"></div>
            </div>
        </section><!--/.contact-->