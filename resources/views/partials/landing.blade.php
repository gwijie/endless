<section id='home' class="hero heroFullscreen bg-parallax" data-jarallax='{"speed": 0.2}' style='background-image: url("{{ url('storage/'.$landing['bg_image']) }}")'>
            <div class='hero-overlay'></div>
                <div class="d-flex align-items-center">
                    <div class="scroll-to hero-mouse"><a data-scroll="" href="#about"><i class="ti-angle-down"></i></a></div>
                    <div class="container">
                        <div class="row">
                            <div class=" col-md-8  offset-md-2 text-center">
                                <h1 class="uppercase" style="color: #ffcc00!important;">{{ setting('site.title') }}</h1>
                                <h4 style="color: white;font-weight: normal!important;" class="col-md-12 ">Home ownership has become an elusive dream for many people in Kenya. 
                                {{ $landing['intro'] }}
                                </h4><br>
                                {!! $landing['call_to_action'] !!}
                            </div>
                        </div>
                </div>
            </div>
</section><!--/.hero-->