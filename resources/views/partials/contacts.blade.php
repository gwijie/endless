<section id="contact" class="pt90 pb60 bg-faded">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4 style="color: #ffcc00!important;">Contact Us</h4>
                        </div>
                    </div>
                </div><!--/section title-->
               <div class="row">
                    <div class="col-md-6 mb30">
                        
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8053468523863!2d36.77419341475401!3d-1.2911279990582993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1a0f29c564a5%3A0xae14d246bc6bd339!2sMethodist+Ministries+Centre!5e0!3m2!1sen!2ske!4v1526860022686" class="mb0" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6" id="hello">
                        <h4 class="text-capitalize">Say Hello!</h4>
                        <hr>
                        <form method="post" action="submit_contact" data-toggle="validator" class="">
                        {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-12">                                 
                                    <div class=" mb20">
                                        <input type="text" name="name" class="form-control" placeholder="Full Name...." required/>
                                    </div>
                                    <div class=" mb20">
                                        <input type="email" name="email" class="form-control" placeholder="Email Address...." required email/>
                                    </div>                          
                                </div>
                                <div class="col-sm-12">
                                    <textarea name="msg" class="form-control mb20" rows="5" placeholder="Message...." required></textarea>                                  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <?php echo Session::get('messages');?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-lg btn-primary">Send Message</button>
                                </div>
                            </div>
                        </form><!--form end-->
                    </div>
                </div>

            </div>
        </section><!--/.contact-->