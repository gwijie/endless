<div class="bg-primary contact-info pt50 pb20">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-home"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Address</h5>
                                {{ $address['address'] }}
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-mobile"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Phone</h5>
                                {{ $address['telephone'] }}
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-email"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Email & website</h5>
                                {{ $address['email'] }}
                            </div>
                        </div>
                    </div><!--/.col-->
                </div>
            </div>
        </div><!--/.contact-info-->