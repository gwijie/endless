                    <div class="row">
                        <div class="col-md-12 mb30">
                            <h3 style="color: #ffcc00!important;">Our Values </h3> 
                        </div>
                        <br>
                        @foreach($values as $value)
                        <div class="col-md-6 mb30 wow zoomIn" data-wow-duration=".5s" data-wow-delay=".2s">
                            <div class="media icon-box">
                                    <div class="d-flex mr-4">
                                        {!! $value->icon !!}
                                    </div>
                                    <div class="media-body">
                                        <h4 class="mt-0 text-capitalize"><span class='text-primary'></span> {{ $value->value }}</h4>
                                        <p class='mb0 text-small'>
                                        {{ $value->description }}
                                        </p>
                                    </div>
                            </div>
                        </div><!--/col-->
                        @endforeach
                    
                    </div>
                