<footer id="footer" class="footer  pt90 pb90">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-twitter"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-google"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-instagram"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-linkedin"></i></a>
                            </li>
                        </ul>
                        <span>&copy; Copyright 2018. All Right Reserved.</span>
                    </div>
                </div>
            </div>
        </footer>
        <!--back to top-->
        <a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="{{ url('js/plugins/plugins.js')}}"></script> 
        <script src="{{ url('js/gems.custom.js')}}"></script> 