
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENDLESS AFRICA | {{ $pageName}} </title>
        <!-- Plugins CSS -->
        <link href="{{ url('css/plugins/plugins.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ url('css/style.css')}}" rel="stylesheet">
    </head>
    <body data-spy="scroll" data-offset="58">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-inverse bg-inverse header-transparent fixed-top" style="background: black!important;">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="http://endlessafrica.digitalatelier.co.ke/images/logo.png" alt="Endless Africa" width="150px;"></a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a data-scroll class="nav-link" href="/">Home</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link {{ $aboutActive or '' }}" href="/about">About</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="/#contact">Contact</a></li>                    
                    <li class="nav-item"><a class="nav-link bg-primary btn-rounded active {{ $surveyActive || '' }}" href="/survey">Take a Test</a></li>
                </ul>
            </div>
        </nav><!--/.navbar-->
        <br><br>