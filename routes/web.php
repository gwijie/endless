<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingPageController@index')->name('landingpage.index');
Route::get('/about', 'AboutPageController@index')->name('aboutpage.index');
//Route::get('/submitted', function () {
//    return view('pages.submitted');
//});

Route::get('/survey', 'SurveyPageController@index')->name('surveypage.index');
Route::get('/survey_report', 'SubmittedPageController@index')->name('submittedpage.index');
Route::post('/submitted', 'SurveyPageController@submitted')->name('submittedpage.submitted');
Route::post('/submit_contact', 'ContactPageController@submit_contact');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
